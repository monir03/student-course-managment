<?php
		
	//Connection of db
	require_once('class.php');
	$pdo = new ConnectionDb();
	
	if( isset($_POST['login']) )
	{

		$email 		= $_POST['email'];
		$password 	= $_POST['password'];

		if( $email =='' || $password=='')
		{
			echo "<h3 style='color: #fff;'>Fill up correctly</h3>";
		}else
		{
			$sql = "SELECT * FROM admin WHERE email='$email'";

			$all_admin = $pdo -> pdo_connection -> prepare($sql);
			
			$all_admin->execute();

			if( $all_admin->rowCount() == 0 )
			{
				echo "<h3 style='color: #fff;'>Invalid login</h3>";
			}else
			{
				header("location:admin.php");
			}

		}
	}
	

?>
	
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Registration form</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.css">
	<link rel="stylesheet" href="css/style.css">
	<style>
		
	</style>
	
</head>
	
<body>

	<div class="area">
		<h2>Admin Login</h2>
		<form action="" method="POST">
			<div class="box">
				<hr>
				<div class="form-group">
					<label for="email">Enter Your Email:</label>
					<input id="email" type="email" name="email" class="form-control bgc">
				</div>
				<div class="form-group">
					<label for="pass">Enter Your Password:</label>
					<input type="password" name="password" class="form-control bgc">
				</div>
				<div class="form-group">
					<input type="submit" name="login" value="Login" class="btn btn-success">
				</div>
			</div>
		</form>
	</div>

</body>
</html>