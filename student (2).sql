-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2017 at 10:59 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `student`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(1, 'monir@gmail.com', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `assign`
--

CREATE TABLE `assign` (
  `st_info_id` int(11) NOT NULL,
  `st_course_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assign`
--

INSERT INTO `assign` (`st_info_id`, `st_course_id`) VALUES
(6, 7),
(12, 1),
(12, 7),
(14, 1),
(14, 7),
(14, 10),
(14, 11),
(14, 12),
(14, 13),
(6, 1),
(6, 7),
(14, 1),
(14, 7),
(14, 13),
(16, 1),
(16, 7),
(16, 10),
(16, 11),
(16, 12),
(16, 13),
(16, 14),
(17, 1),
(13, 1),
(13, 7),
(13, 10),
(13, 11),
(13, 12),
(13, 13),
(13, 14);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `c_id` int(255) NOT NULL,
  `course_title` varchar(255) NOT NULL,
  `course_id` varchar(255) NOT NULL,
  `course_duration` varchar(255) NOT NULL,
  `course_credits` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`c_id`, `course_title`, `course_id`, `course_duration`, `course_credits`) VALUES
(1, 'html', 'p1', '6-moths', '4'),
(7, 'Mysql', 'm1', '6-months', '9'),
(10, 'php', 'p1', '2 months', '5'),
(11, 'css', 'c1', '3-months', '4'),
(12, 'Java', 'j1', '5 months', '6'),
(13, 'C#', 'C#-1', '6-months', '6'),
(14, 'jS', 'js2', '3-months', '4');

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `assign` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `name`, `email`, `password`, `phone`, `assign`) VALUES
(5, 'Mr Jon', 'jon@gmail.com', '343434', '012356694564', 'yes'),
(6, 'Lianne', 'lianne@gmail.com', '66666', '025892541545', 'yes'),
(12, 'hh', 'm@gmail.com', '23232323', '897979', 'no'),
(13, 'mahbuib', 'mahbub@GMAIL.COM', '565', 'uy7878979', 'yes'),
(14, 'morsed', 'mr@gmail.com', '12121', '78978979', 'no'),
(15, 'ponkoj', '', '', '', 'no'),
(16, 'mokamal', '', '', '', 'no'),
(17, 'quader', '', '', '', 'no');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assign`
--
ALTER TABLE `assign`
  ADD KEY `st_info_id` (`st_info_id`),
  ADD KEY `st_course_id` (`st_course_id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `c_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `assign`
--
ALTER TABLE `assign`
  ADD CONSTRAINT `assign_ibfk_1` FOREIGN KEY (`st_info_id`) REFERENCES `info` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assign_ibfk_2` FOREIGN KEY (`st_course_id`) REFERENCES `courses` (`c_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
