
<?php 

	
	//DB connection
	require_once('Class.php');
	$pdo = new ConnectionDb();

	if ( isset($_POST['submit'])) {
		
		$course_name 	 = $_POST['course_name'];
		$course_id 		 = $_POST['course_id'];
		$course_duration = $_POST['course_duration'];
		$course_credits  = $_POST['course_credits'];


		if ( $course_name!='' && $course_id!='' && $course_duration!='' && $course_credits!='') {

			$sql = "INSERT INTO courses(course_title, course_id, course_duration, course_credits) VALUES('$course_name','$course_id', '$course_duration','$course_credits' )";

			$all_course = $pdo -> pdo_connection -> exec($sql);

				if ($all_course) {
					echo "Course Inserted";
				}

		}else{
			echo "Course not inserted";
		}
		
	}


	if ( isset($_GET['action']) && $_GET['action'] =="logout") {
			
			header("location:adminLogin.php");		
	}



?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Student Managment</title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">

</head>
<body>

	<div class="area">
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<ul class="nav navbar-nav">
					<li class="active"><a href="">Add course</a></li>
					<li><a href="assign.php">Course Assign</a></li>
					<li><a href="view-course.php">View Courses</a></li>
					<li><a href="?action=logout"><span class="glyphicon glyphicon-log-out">Logout</span></a></li>
				</ul>
			</div>
		</nav>

		<h2>Add Courses</h2>
		<form action="" method="POST"">
			<div class="box">
				<hr>
				<div class="form-group">
					<label for="name">Enter Course Title:</label>
					<input type="text" name="course_name" id="name" class="form-control bgc">
				</div>
				<div class="form-group">
					<label for="id">Enter Course ID:</label>
					<input id="id" type="text" name="course_id" class="form-control bgc">
				</div>
				<div class="form-group">
					<label for="">Enter Course Duration:</label>
					<input type="text" name="course_duration" class="form-control bgc">
					
				</div>
				<div class="form-group">
					<label for="address">Enter Course Credits:</label>
					<input type="number" name="course_credits" class="form-control bgc">
				</div>
				<div class="form-group">
					<input type="submit" name="submit" value="Add course" class="btn btn-success">
					<input type="reset" name="submit" value="Reset" class="btn btn-info">
				</div>
			</div>
		</form>
	</div>	
	
</body>
</html>

