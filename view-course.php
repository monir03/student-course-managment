<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Students</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.css">
	<link rel="stylesheet" href="css/style.css">

	<style>
		h2{
			margin-top: 30px;
		}
		table tr td, th{
			text-align: center;
		}
		table tr th{
			color: #fff;
		}
	</style>

</head>

<body>

	<?php  

		require_once('Class.php');
		$pdo = new ConnectionDb();

		if (isset($_GET['action'])  && $_GET['action'] =="logout" ) {
			
			header("location:adminlogin.php");
		}
	?>

	<div class="area">
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<ul class="nav navbar-nav">
					<li><a href="admin.php">Add Course</a></li>
					<li><a href="assign.php">Course Assign</a></li>
					<li class="active"><a href="view-course.php">View Courses</a></li>
					<li class=""><a href="view-course.php?action=logout">Logout</a></li>
				</ul>
			</div>
		</nav>

		<h2>All Students</h2>
		<hr>
    	<table class="table table-bordered">
			<tr>
				<th>SL</th>
				<th>Course Name</th>
				<th>View Details</th>
			</tr>
			
	<?php 

			$i=0;

			$sql= "SELECT * FROM courses";

			$all_st_info = $pdo->pdo_connection->prepare($sql);

			$all_st_info->execute();

			$all_st_info ->setFetchMode(PDO::FETCH_ASSOC);

			foreach ( $all_st_info->fetchAll() as $st_info ) {

			$i++;
				
	?>
			<tr>
				<td><?php echo $i ?></td>
				<td><?php echo $st_info['course_title'] ?></td>
				<td><a class="btn btn-success" 

				href="view-student-course.php?id=<?php echo $st_info['c_id'] ?>&name=<?php 

				echo $st_info['course_title']; ?>">

				View</a></td>
			</tr>

	<?php } ?>


		</table>
	</div>

</body>
</html>