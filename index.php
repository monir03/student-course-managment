<?php 
	
	//Db connection
	require_once('class.php');
	$pdo = new ConnectionDb();

	if( $_SERVER['REQUEST_METHOD'] == "POST" && $_POST['submit'] )
	{

		$name 		= $_POST['name'];
		$email 		= $_POST['email'];
		$password 	= $_POST['password'];
		$mobile 	= $_POST['mobile'];

		$sql = "INSERT INTO info ( name, email, password, phone, assign ) VALUES ( '$name', '$email', '$password', '$mobile', 'no' )";

		$all_data = $pdo->pdo_connection->query($sql);


			if ($all_data) {
				echo "Data inserted successfully";
			}
	
			header("location:index.php");
	}


	?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Registration form</title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.css">
	<link rel="stylesheet" href="css/style.css">
	<style>
		.area a{
			display: block;
			margin: 0 auto;
			float: right;
		}
	</style>
	
</head>
	
<body>

	<div class="area">
		<h2>Student Registration</h2>
				<div class="form-group">
					<a class="btn btn-info" href="adminLogin.php"> Admin login</a>
				</div>
		<form action="" method="POST">
			<div class="box">
				<hr>
				<div class="form-group">
					<label for="name">Enter Your Name:</label>
					<input type="text" name="name" id="name" class="form-control bgc">
				</div>
				<div class="form-group">
					<label for="email">Enter Your Email:</label>
					<input id="email" type="email" name="email" class="form-control bgc">
				</div>
				<div class="form-group">
					<label for="pass">Enter Your Password:</label>
					<input type="password" name="password" class="form-control bgc">
					
				</div>
				<div class="form-group">
					<label for="cell">Enter Your Mobile:</label>
					<input id="cell" type="text" name="mobile" class="form-control bgc">
				</div>
				<div class="form-group">
					<input type="submit" name="submit" value="Register" class="btn btn-success">
					<input type="reset" name="submit" value="Reset" class="btn btn-info">
				</div>
			</div>
		</form>
	</div>

</body>
</html>