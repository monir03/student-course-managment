<?php
		
	//Db connection
	require_once('Class.php');
	$pdo = new ConnectionDb();

	if ( isset($_POST['submit']) ) {
		
		$student_name 	= $_POST['student'];
		$availabe_course 	= $_POST['courses'];
		$assign = false;

		if ( !empty($student_name) ) {
			
			$total_course = count($availabe_course);

			for ($i=0; $i<$total_course; $i++) { 

			$sql = "INSERT INTO assign (st_info_id, st_course_id) VALUES (:student_name, :availabe_course)";
				$assign = true;
				$stmt= $pdo->pdo_connection->prepare($sql);
				
				$stmt->bindValue(':student_name',$student_name );
				$stmt->bindValue(':availabe_course',$availabe_course[$i]  );
				$stmt->execute();

			}

			if ( $assign=true) {
				
				$usql= "UPDATE info SET assign = 'yes' WHERE info.id =".$student_name;
	
				$pdo->pdo_connection->query($usql);

				echo "inserted";
			}
		}else{
			echo "not inserted";
		}

	}


	
	if (isset($_GET['action']) && $_GET['action']=="logout") {
		header("location:adminLogin.php");
	}
?>





<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Student Managment</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">

</head>
<body>

	<div class="area">
		<!-- Menu  -->
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<ul class="nav navbar-nav">
					<li><a href="admin.php">Add Course</a></li>
					<li class="active"><a href="">Course Assign</a></li>
					<li><a href="view-course.php">View Courses</a></li>
					<li><a href="?action=logout"><span class="glyphicon glyphicon-log-out">Logout</span></a></li>
				</ul>
			</div>
		</nav>

		<h2>Assain course</h2>
	
	<form action="" method="POST"">
		

		<div class="box">
			<hr>

			<div class="form-group">
				<label for="name">Select Student:</label>

				<select name="student" class="form-control">
					<option value="">Select student</option>
				<?php 

					$sql = "SELECT * FROM info WHERE assign='no'";

					$all_q = $pdo->pdo_connection->prepare($sql);
					$all_q->execute();

					$res = $all_q->setFetchMode(PDO::FETCH_ASSOC);

					foreach ( $all_q->fetchAll() as $std ) { ?>
						<option value="<?php echo $std['id']?>"><?php echo  $std['name'] ?></option>

				<?php 	} ?>

				
					


				</select>
			</div>

			<div class="form-group">
				<label for="name">Availabe Courses:</label>
			<?php

                $sql="SELECT * FROM courses";

                $all_courses= $pdo -> pdo_connection->prepare($sql);

                $all_courses->execute();

                $res=$all_courses->setFetchMode(PDO::FETCH_ASSOC);

                foreach($all_courses->fetchAll() as $course){ ?>

                    <label class="checkbox-inline"><input type="checkbox" value="<?php echo $course['c_id']?>" name="courses[]"><?php echo $course['course_title']?></label>

            <?php } ?>
			</div>

			<div class="form-group">
				<input type="submit" name="submit" value="Assain course" class="btn btn-success">
				<!-- <input type="reset" name="submit" value="Reset" class="btn btn-info"> -->
			</div>

		</div>

	</form>
	</div>	
	
</body>
</html>