<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Students</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.css">
	<link rel="stylesheet" href="css/style.css">

	<style>
		h2{
			margin-top: 30px;
			color: #fff;
		}
		table tr td, th{
			text-align: center;
		}
		.area table tr th{
			color: #fff;
		}
	</style>

</head>

<body>

	<?php  

		require_once('Class.php');
		$pdo = new ConnectionDb();

		if (isset($_GET['action']) && $_GET['action']=="logout") {
		header("location:adminLogin.php");

		}

		if ( isset($_GET['id']) && isset($_GET['name']) ) {
			
			$course_id 		= $_GET['id'];
			$student_name 	= $_GET['name'];
		}


	?> 

	<div class="area">
		<!-- Menu  -->
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<ul class="nav navbar-nav">
					<li><a href="admin.php">Add Course</a></li>
					<li><a href="assign.php">Course Assign</a></li>
					<li><a href="view-course.php">View Courses</a></li>
					<li><a href="?action=logout"><span class="glyphicon glyphicon-log-out">Logout</span></a></li>
				</ul>
			</div>
		</nav>

		<h2>Course name:</h2>
		<hr>
    	<table class="table table-bordered">
			<tr>
				<th>SL</th>
				<th>Student name</th>
				<th>Student Email</th>
				<th>Course name</th>
			</tr>
			
		<?php 

			$i=0;

$sql ="SELECT name,email, course_title FROM info, courses, assign WHERE courses.c_id = assign.st_course_id AND info.id = assign.st_info_id AND assign.st_course_id =".$course_id;

			$all_data = $pdo->pdo_connection->prepare($sql);
			$all_data->execute();
			$all_data->setFetchMode(PDO::FETCH_ASSOC);

			foreach ($all_data->fetchAll() as $single) { 

				$i++;

				?>

				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $single['name'] ?></td>
					<td><?php echo $single['email'] ?></td>
					<td><?php echo $single['course_title'] ?></td>
				</tr>
				
			<?php } ?>


		</table>
		<a class="btn btn-info" href="admin.php">Return to Dashboard?</a>
	</div>

</body>
</html>